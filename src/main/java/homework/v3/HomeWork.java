package homework.v3;


/**
 * Задание
 * 1) Зайти в пакет homework.v3.entity.
 * Определить какой из классов является корневым элементом
 * Проинициализировать класс минимум пятью элементами
 * Сохранить в файл с именем homework.parameters.json в формате JSON корневой объект
 * 2) Считать получившийся homework.parameters.json в память
 * 3) Сериализовать его с помощью встроенного механиза сериализации в файл с именем homework.parameters.ser
 * 4) Сериализовать его с использованием интерфейса Externalizable в файл с именем homework.parameters.exter
 * 5) Считать данные из файла homework.parameters.ser в память и сохранить в формате JSON в файл с именем homework.result.ser.parameters.json
 * 6) Считать данные из файла homework.parameters.exter в память и сохранить в формате JSON в файл с именем homework.result.exter.parameters.json
 * 7) Убедиться, что файлы homework.result.ser.parameters.json и  homework.result.exter.parameters.json
 * совпадают с homework.parameters.json
 * 8) Составленную в п.1 сущность представить в виде xsd-схемы и
 * выполнить генерацию классов аналогично классам из пакета classwork.entity.jaxb
 * * можно сделать и с json-схемой, принципиально механизм не поменяется.
 * */

public class HomeWork {
}
